#include "stdlib.h"
#include "stdio.h"
#include "stdbool.h"
#include "unistd.h"
#include "string.h"
#include "primorum.h"

bool generate_primes(int pfd, unsigned long max)
{
    int niter, piter, biter = 0;
    unsigned char ppack = 0;
    bool isprime;
    for (niter = 2; niter < max; niter++) {
        isprime = true;
        for (piter = 2; piter * piter <= niter; piter++) 
            if (niter % piter == 0) {
                isprime = false;
                break;
            }
        if (isprime) {
            while (biter < niter / (30 * sizeof(unsigned char))) {
                if (write(pfd, &ppack, sizeof(unsigned char)) == -1) {
                    fprintf(stderr, "Unable to write output file..\n");
                    return false;
                }
                ppack = 0;
                biter++;
            }
            primorum_pack(&ppack, niter % 30);
        }
    }
    return true;
}

void read_primes(int pfd)
{
    unsigned char ppack;
    unsigned char unpacked[PRIMORUM_PACK_LENGTH];
    int biter;
    unsigned long primebase = 0;
    while (read(pfd, &ppack, sizeof(unsigned char)) > 0) {
        memset(unpacked, 0, PRIMORUM_PACK_LENGTH * sizeof(unsigned char));
        primorum_unpack(&ppack, unpacked);
        for (biter = 0; biter < PRIMORUM_PACK_LENGTH; biter++)
            if (unpacked[biter] != 0)
                printf("%lu\n", primebase + unpacked[biter]);
        primebase += 30 * sizeof(unsigned char);
    }
}


int main(int argc, char* argv[])
{
    int pfile;
    unsigned long maxn;
    char* endstr;
    char fname[] = "/tmp/primes.XXXXXX";
    if (argc < 2) {
        fprintf(stderr, "Usage: %s NUM\n", argv[0]);
        return EXIT_FAILURE;
    }
    pfile = mkstemp(fname);
    if (pfile == -1) {
        fprintf(stderr, "Unable to create temporary file..\n");
        return EXIT_FAILURE;
    }
    maxn = (unsigned long) strtol(argv[1], &endstr, 0);
    if (*argv[1] == '\0' || *endstr != '\0') {
        fprintf(stderr, "Invalid argument..\n");
        return EXIT_FAILURE;
    }
    if (!generate_primes(pfile, maxn)) {
        fprintf(stderr, "Error while generating primes..\n");
        return EXIT_FAILURE;
    }
    if (lseek(pfile, 0, SEEK_SET) == -1) {
        fprintf(stderr, "Cannot get back to the beginning of the file..\n");
        return EXIT_FAILURE;
    }
    read_primes(pfile);
    if (close(pfile) == -1) {
        fprintf(stderr, "Error while closing file..\n");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
